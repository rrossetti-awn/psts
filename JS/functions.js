$(document).ready(function () {
  stickynav();
  materialButtonLike();
  hamburgerToggle();
  scroller();
  closeMenu();
  titleLoad();
});

// INITIALISING SCROLLING FUNCTIONS
$(document).scroll(function () {
  parallax();
});

$(window).resize(function () { });

function scroller(duration) {
  //Scroll to div function
  $("a[href^='#']").on("click", function (event) {
    var target = $($(this).attr("href"));

    if (target.length) {
      event.preventDefault();
      $("html, body").animate(
        {
          scrollTop: target.offset().top,
        },
        duration
      );
    }
  });
}

const date = new Date();
let year = date.getFullYear();

document.querySelector(".thisYear").innerText = `@${year}`;

// -----------------
//      NAVBAR
// -----------------

// NAVBAR CHANGE ON SCROLL FROM TOP
function stickynav() {
  let toggleAffix = function (affixElement, scrollElement, wrapper) {
    let height = affixElement.outerHeight(),
      top = wrapper.offset().top;
    if ($(".navbar").hasClass("affix")) {
      $(".logo").attr("src", "assets/svg/PSTS-logo.svg");
      $(".navbar-nav").addClass("my-2");
      $(".navbar-collapse .collapse-btn").show();
      if (window.innerWidth <= 900) {
        $(".logo").attr("src", "assets/svg/PSTS-icon.svg");
        $(".navbar-collapse .collapse-btn").hide();
      }
    } else {
      $(".logo").attr("src", "assets/svg/PSTS-logo-alt.svg");
      $(".navbar-collapse .collapse-btn").hide();
      $(".navbar-collapse .non-collapse-btn").show();
      $(".navbar-nav").removeClass("my-2");
      if (window.innerWidth <= 900) {
        $(".logo").attr("src", "assets/svg/PSTS-icon-alt.svg");
      }
    }
    if (scrollElement.scrollTop() >= top) {
      wrapper.height(height - height);
      affixElement.addClass("affix alt-nav navbar-light");
      affixElement.removeClass("navbar-dark");
    } else {
      affixElement.removeClass("affix alt-nav navbar-light");
      affixElement.addClass("navbar-dark");
      wrapper.height("auto");
    }
  };

  $('[data-toggle="affix"]').each(function () {
    let ele = $(this),
      wrapper = $("<div></div>");

    ele.before(wrapper);
    $(window).on("scroll resize", function () {
      toggleAffix(ele, $(this), wrapper);
    });

    // init
    toggleAffix(ele, $(window), wrapper);
  });
}

// NAVBAR HAMBURGER MENU TOGGLE ANIMATION
function hamburgerToggle() {
  $(".navbar-toggle").click(function () {
    $(".hamb").toggleClass("active");
  });
}

//MOBILE MENU COLLAPSE BACK ON CLICK (TO USE WITH SCROLL-TO-POSITION ANIMATION)
function closeMenu() {
  $(".navbar-nav li a")
    .not(".dropdown-toggle")
    .on("click", function () {
      $(".navbar-ex1-collapse").removeClass("in");
      $(".hamb").removeClass("active");
    });
}

// -----------------
//     BUTTONS
// -----------------

// MATERIAL DESIGN BUTTON RIPPLE EFFECT
const materialButtonLike = () => {
  //creating a style object for the ripple effect
  function RippleStyle(width, height, posX, posY) {
    this.width = width <= height ? height : width;
    this.height = width <= height ? height : width;
    this.top = posY - this.height * 0.5;
    this.left = posX - this.width * 0.5;
  }

  $(".btn").on("mousedown", function (e) {
    //appending an element with a class name "btn-ripple"
    let rippleEl = $('<span class="btn-ripple"></span>').appendTo(this);

    //getting the button's offset position
    let pos = $(this).offset();

    //get the button's width and height
    let width = $(this).outerWidth();
    let height = $(this).outerHeight();

    //get the cursor's x and y position within the button
    let posX = e.pageX - pos.left;
    let posY = e.pageY - pos.top;

    //adding a css style to the ripple effect
    let rippleStyle = new RippleStyle(width, height, posX, posY);
    rippleEl.css(rippleStyle);
  });

  //this event listener will be triggered once the ripple animation is done
  $(".btn").on(
    "animationend webkitAnimationEnd oanimationend MSAnimationEnd",
    ".btn-ripple",
    function () {
      $(this).remove();
    }
  );
};

// -----------------
//    ANIMATIONS
// -----------------

// TITLE ANIMATION ON PAGE LOAD
function titleLoad() {
  $(".parallax .container, .section-load").css({
    top: "0",
  });
  $(".section-load .card .row").css({
    transform: "translateY(0)",
    opacity: "1",
  });
  $(".parallax").css({
    transform: "translateY(0)",
  });
  $(".parallax .container").css({
    opacity: "1",
  });
}

// ----------------------------------
//  PARALLAX EFFECT ON HEADER (ALL PAGES)
// ----------------------------------
function parallax() {
  let wScroll = $(this).scrollTop();

  if ($("header").offset().top - $(window).height() < wScroll) {
    $("header").css({
      transform: "translate(0px, " + wScroll / 48 + "%)",
    });
  }
}

// ----------------------------------
//    CARD BLURB EXPAND ON CLICK
// ----------------------------------
$(".picture-card-left, .picture-card-right").on("click", function () {
  const $this = $(this),
    fader = $this.find(".text-fader"),
    container = $this.find(".card-blurb-container"),
    readMore = $this.find(".read-more");

  readMore.toggleClass("card-blurb-more card-blurb-less");
  if (readMore.is(".card-blurb-more")) {
    fader.css({
      height: "120",
    });
    container.css({
      maxHeight: "180px",
    });
    readMore.text("...read more");
  } else if (readMore.is(".card-blurb-less")) {
    fader.css({
      height: "0",
    });
    container.css({
      maxHeight: "1000px",
    });
    readMore.text("...read less");
  }
});

// ----------------------------------
//    CARD SESSION CONTENT ON CLICK
// ----------------------------------
$(".session-btn").on("click", function (e) {
  e.stopPropagation();
  const $this = $(this),
    pos = $this.parent().index(),
    siblings = $this.parent().siblings().children(),
    textContainer = $this
      .parentsUntil(".session-row")
      .parent()
      .siblings()
      .find(".card-blurb-container"),
    title = textContainer.siblings("h3"),
    readMore = textContainer.siblings(".read-more"),
    bar = title.siblings(".p-bar"),
    backBtn = title.siblings(".card-back-link");

  $this.addClass("session-active");
  siblings.removeClass("session-active");

  if (pos === 0) {
    textContainer.html(
      "<p>In the first session we start at the beginning introducing the foundation to selling.</p> <p>This combines the skill sets and the art of open-ended questions, the techniques on how to listen and how to negotiate. We create the environment where the participant can role play these skills to better enhance their selling ability.</p> "
    );
    title.text("Product and salesmanship - Session 1");
    textContainer.css({
      maxHeight: "600px",
      marginBottom: "60px",
    });
    bar.css({
      width: "0",
    });
    setTimeout(() => {
      bar.css({
        display: "none",
      });
      backBtn.removeClass("d-none");
    }, 300);

    readMore.hide();
  }
  if (pos === 1) {
    textContainer.html(
      "<p>In the second session we drill down further and look at the understanding of negotiation introducing body language skills, communication skills, the use of the tone of a voice and the closing techniques within the sales process. </p> <p>We expand on the first session, so we further enhance the participant selling ability.</p> "
    );
    title.text("Product and salesmanship - Session 2");
    textContainer.css({
      maxHeight: "600px",
      marginBottom: "60px",
    });
    bar.css({
      width: "0",
    });
    setTimeout(() => {
      bar.css({
        display: "none",
      });
      backBtn.removeClass("d-none");
    }, 300);
    readMore.hide();
  }
  if (pos === 2) {
    textContainer.html(
      "<p>The third session combines both the first and the second sessions and becomes the “selling road to a sale.” It is in this session we bring it all together, utilising your own road to a sale which we personalise in this last session to fit with your business process. We also expand on the skills sets, drilling down on what makes a person want to buy and understanding the difference of a “tell sell” and a “adaptive sell.”, included is the understanding of a SWOT analysis as a selling tool.</p><p>Throughout each session we tailor product quizzes based on your products to further build the confidence of the participant. These sessions also have the added value of support as we provide continuous follow up via zoom/phone support which enables the participant to review his/her progress end to fine tune their selling skills over the three courses we continue to build on each step.</p><p>We can revise and revisit any aspect of the course to ensure the selling skills are being absorbed by the participant.</p><p>With all the courses provided by the PSTS Group, it’s essential we modify the content for your business. The fundamentals of the course remain but the true value is always personalising to your specific requirements.</p>"
    );
    title.text("Product and salesmanship - Session 3");
    textContainer.css({
      maxHeight: "1000px",
      marginBottom: "10px",
    });
    bar.css({
      width: "0",
    });
    setTimeout(() => {
      bar.css({
        display: "none",
      });
      backBtn.removeClass("d-none");
    }, 300);
    readMore.show().css({ marginBottom: "80px !important" });
  }
});

$(".card-back-link").on("click", function (e) {
  e.stopPropagation();
  const $this = $(this),
    title = $this.siblings("h3"),
    textContainer = $this.siblings(".card-blurb-container"),
    readMore = textContainer.siblings(".read-more");

  title.text("Product and salesmanship");
  textContainer
    .css({ maxHeight: "180px", marginBottom: "0" })
    .html(
      "<div class='text-fader text-dimmer position-absolute'></div><p>In the world that is becoming reliant on digital media and digital platforms to research products and prices, the new generation have lost the simple art of selling.</p><p>Below are 3 sessions specifically designed to ensure when customer comes into your business, your sales team understand how important they are. That they comprehend that every lead is vital, and every lead is valuable.</p><p>In most cases a human will want to talk with a human and we put the FUN back into selling which stands for“Fundamentally Understanding Negotiation”.</p>"
    );

  readMore.show().text("...read more");
  $this.before("<div class='p-bar mb-2' style'width: 0px'></div>");
  $this.addClass("d-none");
  $(".session-btn").removeClass("session-active");
});

// -----------------
//  FORM VALIDATION
// -----------------
let isNameValid = false;
let isEmailValid = false;
let isMessageValid = false;

function checkName() {
  const text = $(".check-name").val() !== "" ? true : false;
  const whiteSpace = $.trim($(".check-name").val()) !== "" ? true : false;
  if (!text || !whiteSpace) {
    $(".check-name").css({ borderBottom: "1px solid var(--text-error)" });
    $(".name-err").removeClass("d-none");
    isNameValid = false;
    validateForm();
    return false;
  }
  $(".check-name").css({ borderBottom: "2px solid var(--main-medium)" });
  $(".name-err").addClass("d-none");
  isNameValid = true;
  validateForm();
  return true;
}

function checkEmail(e) {
  let res = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const text = $(".check-email").val() !== "" ? true : false;
  const whiteSpace = $.trim($(".check-email").val()) !== "" ? true : false;
  if (!text || !whiteSpace) {
    $(".check-email").css({ borderBottom: "1px solid var(--text-error)" });
    $(".email-err")
      .removeClass("d-none")
      .text("We can't contact you if this field is empty");
    isEmailValid = false;
    validateForm();
    return false;
  }

  if (!res.test(e.value)) {
    $(".check-email").css({ borderBottom: "1px solid var(--text-error)" });
    $(".email-err").removeClass("d-none").text("Please provide a valid email");
    isEmailValid = false;
    validateForm();
    return false;
  }
  $(".check-email").css({ borderBottom: "2px solid var(--main-medium)" });
  $(".email-err").addClass("d-none");
  isEmailValid = true;
  validateForm();
  return true;
}

function checkMessage() {
  const text = $(".check-message").val() !== "" ? true : false;
  const whiteSpace = $.trim($(".check-message").val()) !== "" ? true : false;
  if (!text || !whiteSpace) {
    $(".check-message").css({ borderBottom: "1px solid var(--text-error)" });
    $(".msg-err")
      .removeClass("d-none")
      .text("Please provide a short description for your enquiry");
    isMessageValid = false;
    validateForm();
    return false;
  }
  $(".check-message").css({ borderBottom: "2px solid var(--main-medium)" });
  $(".msg-err").addClass("d-none");
  isMessageValid = true;
  validateForm();
  return true;
}

function validateForm() {
  if (isNameValid && isEmailValid && isMessageValid) {
    $(".send-form-btn").prop("disabled", false);
  } else {
    $(".send-form-btn").prop("disabled", true);
  }
}


