$(function () {
  $("#contact-form").on("submit", function (e) {
    if (!e.isDefaultPrevented()) {
      var url = "/php/contact.php";

      $.ajax({
        type: "POST",
        url: url,
        data: $(this).serialize(),
        success: function (data) {
          var messageAlert = "alert-" + data.type;
          var messageText = data.message;

          var alertBox =
            '<div class="alert ' +
            messageAlert +
            ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
            messageText +
            "</div>";
          if (messageAlert && messageText) {
            $(".messages").fadeIn();
            $("#contact-form").find(".messages").html(alertBox);
            $("#contact-form")[0].reset();
            $(".send-form-btn").prop("disabled", true);
            setTimeout(() => {
              $(".messages").fadeOut("slow");
            }, 6000);
          }
        },
      });
      return false;
    }
  });
});
