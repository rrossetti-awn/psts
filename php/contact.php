<?php

// configure
$html = file_get_contents('../templates/PSTS-Enquiry-Details.htm');
$name = str_replace('[name]', $_POST['name'], $html);
$userEmail = str_replace('[email]', $_POST['email'], $name);
$userSubject = str_replace('[subject]', $_POST['subject'], $userEmail);
$userMessage = str_replace('[message]', $_POST['message'], $userSubject);
$from = 'support@pstsgroup.com.au';
$sendTo = 'abird@pstsgroup.com.au';
$subject = 'New Message Enquiry';
$fields = array('name' => 'Name', 'subject' => 'Subject', 'email' => 'Email', 'message' => 'Message'); // array variable name => Text to appear in email
$okMessage = 'Your enquiry was submitted. Thank you, we will get back to you soon!';
$errorMessage = 'A connection error occurred. Please try sending again';
$headers = "From: " . $from. "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
// let's do the sending

$customerHtml = file_get_contents("../templates/PSTS-confirmation-email.htm");
$customerName = str_replace('[name]', $_POST['name'], $customerHtml);
$sendToCustomer = $_POST['email'];
$customerSubject = 'Thanks for enquirying to PSTS Group';

try
{


    mail($sendTo, $subject, $userMessage, $headers);
    mail($sendToCustomer, $customerSubject, $customerName, $headers);

    $responseArray = array('type' => 'success', 'message' => $okMessage);
}
catch (\Exception $e)
{
    $responseArray = array('type' => 'danger', 'message' => $errorMessage);
}

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $encoded = json_encode($responseArray);

    header('Content-Type: application/json');

    echo $encoded;
}
else {
    echo $responseArray['message'];
}
